$(document).ready(function () {
    function header_menu() {
        var hamburger = $('.hamburger');
        var container = $('.header-menu__container'),
            body = $('body');
        hamburger.click(function () {
            if (hamburger.hasClass('active')) {
                hamburger.removeClass('active');
                container.removeClass('active');
                body.removeClass('no-scroll');
            } else {
                hamburger.addClass('active');
                container.addClass('active');
                body.addClass('no-scroll');
            }
        });
        $(window).on('scroll', function () {
            if ($(window).scrollTop() > 33) {
                $('body').addClass('fixed');
            } else {
                $('body').removeClass('fixed');
            }
        })
        $('.header-menu__link-drop-down').each(function () {
            var $this = $(this);
           if($(window).width() > 991){

           }else{
               $this.click(function (e) {
                   var menu = $(this).find('.header-submenu__container');
                   var menu_height = menu.find('.header-submenu').outerHeight();
                   if(($this).hasClass('active')){
                       $this.removeClass('active');
                       menu.css({'height':0});
                   }else{
                       $this.addClass('active');
                       menu.css({'height':menu_height + 20});
                   }
               })
           }
        })
        $(document).mouseup(function (e){
            var div =   $('.header-submenu__container'),
                div2 = $('.header-menu__link-drop-down');
            if (!div.is(e.target) && div.has(e.target).length === 0 &&
                !div2.is(e.target) && div2.has(e.target).length === 0) {
                $('.header-submenu__container').removeClass('active');
                $('.header-menu__link-drop-down').removeClass('active');
            }
        });
    }

    header_menu();

    $('.cost-form__input-num').on('keyup', function(e) {
        if($(this).hasClass('cost-form__input-num-1')){
            if($(this).val() < 1){
                $(this).val(1)
            }
        }else{
            if($(this).val() < 0){
                $(this).val(0)
            }
        }

    });



    function table_price() {
        var hiddenMenuContainer = $('.category-container__table-hidden'),
            hiddenMenu = $('.category-container__table-height'),
            hiddenMenuHeight = hiddenMenu.height(),
            activeElementClick = $('.active-element'),
            activeElementId = $('#table-date');

        activeElementClick.on('click', function () {
            var $this = $(this);
            if ($this.hasClass('click')) {
                $this.removeClass('click');
                hiddenMenuContainer.css({'height': 0});
            } else {
                $this.addClass('click');
                hiddenMenuContainer.css({'height': hiddenMenuHeight + 36});
            }
        });
        $(document).mouseup(function (e) {
            if (!hiddenMenuContainer.is(e.target) && hiddenMenuContainer.has(e.target).length === 0 && !activeElementClick.is(e.target) && activeElementClick.has(e.target).length === 0) {
                activeElementClick.removeClass('click');
                hiddenMenuContainer.css({'height': 0});
            }
        });
        hiddenMenuContainer.on('click', '.category-container__table-th', function () {
            var $this = $(this),
                dataAttr = $this.data('date');
            $('[data-date_price~="' + dataAttr + '"]').addClass('active').siblings().removeClass('active');
            activeElementId.empty();
            $this.find('span').clone().appendTo(activeElementId);
            $this.addClass('active').siblings().removeClass('active');
            activeElementClick.removeClass('click');
            hiddenMenuContainer.css({'height': 0});
        });
    }

    if ($(window).width() < 768) {
        table_price();
    }

    function form_cost() {
        var num = 0;
        $('.cost-form__num').each(function () {
            var $this = $(this),
                $thisPlus = $this.find('.cost-form__num-sign-plus'),
                $thisMinus = $this.find('.cost-form__num-sign-minus'),
                $thisInput = $this.find('.cost-form__input-num'),
                $thisInputPlaceholder = $thisInput.attr('placeholder'),
                num = 0;
            $thisPlus.on('click', function () {
                num = Number($thisInputPlaceholder) + 1;
                $thisInput.val(num);
                $thisInputPlaceholder = Number($thisInputPlaceholder) + 1;
            })
            $thisMinus.on('click', function () {
                num = Number($thisInputPlaceholder) - 1;
                $thisInput.val(num);
                $thisInputPlaceholder = Number($thisInputPlaceholder) - 1;

                if($thisInput.hasClass('cost-form__input-num-1')){
                    if($thisInput.val() < 1){
                        $thisInput.val(1)
                        $thisInputPlaceholder = 1
                    }
                }else{
                    if($thisInput.val() < 0){
                        $thisInput.val(0)
                        $thisInputPlaceholder = 0
                    }
                }
            })
        })
    }

    form_cost();

    function top() {
        $(window).scroll(function () {
            if($(window).scrollTop() > 100){
                $('.arrow-top__desktop').addClass('active');
            }else{
                $('.arrow-top__desktop').removeClass('active');
            }
        })
        $('.arrow-top, .arrow-top__desktop').on('click', function () {
            $('html, body').animate({scrollTop: 0}, 500);
            return false;
        });
    }
    top();

    $('.number-items__slider').on('click', 'a', function () {
           setTimeout(function () {
               $('.number-slider').slick('setPosition');
           },200)
    })

    function anchor() {
        $(".anchor-container").on("click","a", function (event) {
            event.preventDefault();
            var id  = $(this).attr('href'),
                top = $(id).offset().top;
            console.log(top)
            $(this).addClass('active').siblings().removeClass('active');

            if($(window).width() < 600){
                $('body,html').animate({scrollTop: top - 300}, 1500);
            }else{
                $('body,html').animate({scrollTop: top}, 1500);
            }
        });
        $('.number-item__default').each(function () {
            $(this).on('click', function () {
                var $this = $(this),
                    $thisParent = $this.closest('.number-items'),
                    $thisSpan = $this.find('span'),
                    $thisHiddenContainer = $this.next('.number-items__container-hid'),
                    $thisContainer = $thisHiddenContainer.find('.number-items__container'),
                    $thisContainerHeight = $thisContainer.height();
                console.log($thisContainerHeight)
                if($thisParent.hasClass('active')){
                    $thisParent.removeClass('active');
                    $thisHiddenContainer.css({'height':0})
                }else{
                    $thisParent.addClass('active');
                    $thisHiddenContainer.css({'height':$thisContainerHeight + 4})
                }
                $thisContainer.on('click', 'a', function () {
                    $thisParent.removeClass('active');
                    $thisHiddenContainer.css({'height':0})
                    $thisSpan.html($(this).html());
                })
            })
        })
    }

    function slick() {
        $('.slider-big').slick({
            dots: true,
            arrows: true,
            prevArrow: '<div class="slick-arrow"><div class="slick-prev"></div></div>',
            nextArrow: '<div class="slick-arrow"><div class="slick-next"></div></div>',
        });
        $('.gallery-small').slick({
            dots: false,
            arrows: true,
            prevArrow: '<div class="slick-arrow"><div class="slick-prev"></div></div>',
            nextArrow: '<div class="slick-arrow"><div class="slick-next"></div></div>',
            responsive: [
                {
                    breakpoint: 1100,
                    settings: {
                        dots: true,
                    }
                },
            ]
        });
        $('.specialist-slider').slick({
            dots: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<div class="slick-arrow"><div class="slick-prev"></div></div>',
            nextArrow: '<div class="slick-arrow"><div class="slick-next"></div></div>',
            responsive: [
                {
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                    }
                },
            ]
        });
        $('.reviews-slider').slick({
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<div class="slick-arrow"><div class="slick-prev"></div></div>',
            nextArrow: '<div class="slick-arrow"><div class="slick-next"></div></div>',
        });
        $('.slider-modal').slick({
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<div class="slick-arrow"><div class="slick-prev"></div></div>',
            nextArrow: '<div class="slick-arrow"><div class="slick-next"></div></div>',
        });
        $('.number-slider, .number-sliders').slick({
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            infinite: false,
        });
    }

    if($(window).width() < 1000){
        $(".entertainment-item").removeAttr('data-toggle');
        $("a.entertainment-item").click(function (e) {
            e.preventDefault();
        })
    }else{
        $("a.entertainment-item").click(function (e) {
            e.preventDefault();
            setTimeout(function () {
                $('.slider-modal').slick('setPosition');
            }, 200)
        })
    }

    if ($('div').hasClass('map-container')) {
        ymaps.ready(function () {
            var myMap = new ymaps.Map('map', {
                    center: [44.064013, 43.060635],
                    zoom: 13,
                    controls: ['zoomControl']
                }, {
                    searchControlProvider: 'yandex#search'
                }),

                myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                    hintContent: 'Санаторий Лесная Поляна',
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: 'images/map.png',
                    // Размеры метки.
                    iconImageSize: [36, 46],
                });
            myMap.behaviors.disable('scrollZoom');
            myMap.geoObjects.add(myPlacemark);
        });


    }

    $('[data-fancybox="images"]').fancybox({
        loop: true,
    });

    function interesting_block() {
        var interesting_item_height = 0;
        $('.interesting-item').each(function () {
            console.log($(this).outerHeight());
            if ($(this).outerHeight() > interesting_item_height) {
                interesting_item_height = $(this).outerHeight();
            }
        });
        $('.interesting-item').css({'height': interesting_item_height});
    }

    if ($(window).width() > 767) {
        interesting_block();
    }



    $('.phone').mask('+7 (999) - 999-99-99')

    $('#date').glDatePicker(
        {
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            dowNames: ['П', 'В', 'С', 'Ч', 'П', 'С', 'В'],
            onClick: function (target, cell, date) {
                target.val(date.getDate() + ' / ' +
                    (Number( date.getMonth() + 1) < 10 ? '0'+ Number( date.getMonth() + 1) : Number( date.getMonth() + 1)) + ' / ' +
                    date.getFullYear());
            }
        }
    );



    $('select').styler({
        selectSmartPositioning: false,
        singleSelectzIndex: '1',
    });


    $('.bottom-slider').click(function () {
        $('html, body').animate({scrollTop:$(window).height() - 170}, 'slow');
    })

    slick();
    anchor();
})